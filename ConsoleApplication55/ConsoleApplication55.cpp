﻿#include <stdio.h>

int main(void)
{
    int counter = 1, timeless;
    long int number, decimal = 0;


    printf("Input a number in binary arithmetic : ");
    scanf_s("%d", &number);
    while (number != 0)
    {
        timeless = number % 10 * counter;
        decimal = decimal + timeless;
        number = number / 10;
        counter *= 2;
    }
    printf("Now it was converted into decimal numerical system : %d.\n", decimal);
    return(0);
}